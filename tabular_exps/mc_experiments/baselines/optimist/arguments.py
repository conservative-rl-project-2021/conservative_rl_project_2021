import argparse

def get_args():
    
    # Easily add a boolean argument to the parser with default value
    def add_bool_arg(parser, name, default=False):
        group = parser.add_mutually_exclusive_group(required=False)
        group.add_argument('--' + name, dest=name, action='store_true')
        group.add_argument('--no-' + name, dest=name, action='store_false')
        parser.set_defaults(**{name: default})

    # Command line arguments
    parser = argparse.ArgumentParser(description='Conservative OPTIMIST')
    parser.add_argument('--seed', help='RNG seed', type=int, default=0)
    parser.add_argument('--seed_min', type=int, default=0)
    parser.add_argument('--seed_max', type=int, default=5)
    parser.add_argument('--env', type=str, default='MountainCarContinuous-v0') # LQG1D-v0 before
    parser.add_argument('--horizon', type=int, default=300)
    parser.add_argument('--bound_type', type=str, default='max-renyi')
    parser.add_argument('--filename', type=str, default='progress')
    parser.add_argument('--logdir', type=str, default='logs')
    parser.add_argument('--mu_init', type=float, default=None)  # LQG only
    parser.add_argument('--std_init', type=float, default=None)  # LQG only
    parser.add_argument('--delta', type=float, default=0.2)
    parser.add_argument('--drho', type=float, default=1)
    parser.add_argument('--njobs', type=int, default=-1)
    parser.add_argument('--policy', type=str, default='linear')
    parser.add_argument('--max_iters', type=int, default=5000)
    parser.add_argument('--max_offline_iters', type=int, default=10)
    parser.add_argument('--render_after', type=int, default=None)
    parser.add_argument('--gamma', type=float, default=1) # 0.99 before
    parser.add_argument('--multiple_init', type=int, default=None)
    parser.add_argument('--plot_bound', type=int, default=None)  # 1-2-->1D-2D
    parser.add_argument('--grid_size_1d', type=int, default=0)
    parser.add_argument('--mu_min', type=float, default=[-1, 0])
    parser.add_argument('--mu_max', type=float, default=[1, 20])
    parser.add_argument('--delta_t', type=str, default='continuous') # None before, but seems to control the discretization or not
    parser.add_argument('--k', type=int, default=3)  # must be>=2
    
    # Conservative parameters
    parser.add_argument('--alpha', type=int, default=0.5)
    parser.add_argument('--baseline_weights', type=str, default='baseline/checkpoint.pkl')
    add_bool_arg(parser, 'conservative', default=True)
    
    # Other boolean args
    add_bool_arg(parser, 'bounded_policy', default=True)
    add_bool_arg(parser, 'trainable_std', default=False)
    add_bool_arg(parser, 'truncated_mise', default=True)
    add_bool_arg(parser, 'experiment', default=False)
    add_bool_arg(parser, 'find_optimal_arm', default=False)
    add_bool_arg(parser, 'plot_ess_profile', default=False)
    add_bool_arg(parser, 'rescale_ep_return', default=False)
    add_bool_arg(parser, 'save_weights', default=False)

    args = parser.parse_args(args=[])
    
    return args

