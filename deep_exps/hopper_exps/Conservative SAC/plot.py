import os
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import pdb


"""
Script to plot the curves of performance and conservative condition of the paper for Conservative SAC experiment.
"""

#util function to list all the files containing the pattern "regex"
def findfiles(path, regex):
    res = []
    for root, dirs, fnames in os.walk(path):
        for fname in fnames:
            if regex in fname:
                res.append(os.path.join(root, fname))
    return res

x_length = 800 #length of x axis plots
x_length_cons = 600 #length of x axis plots
alpha = 0.2 #conservative factor

#load data without conservative condition
path_no_conservative = '/home/mathias/Bureau/rlkit-master/data'
files_no_conservative = findfiles(path_no_conservative, 'progress.csv')

#load data with conservative condition
path_conservative = '/home/mathias/Bureau/skim/data'
files_conservative = findfiles(path_conservative, 'progress.csv')


##########Plot performance ###############


#get exploration return of the non-conservative version for every run
rewards, epochs = [], []
for file in files_no_conservative:
    df = pd.read_csv(file)
    rewards.append(df['exploration/Returns Mean'].values[:x_length])
    epochs.append(df['Epoch'].values[:x_length])

rewards = np.array(rewards)
rewards_mean = np.mean(rewards,axis=0)
rewards_std = np.std(rewards,axis=0)

#get exploration return of the conservative version for every run
rewards_cons, epochs_cons = [], []
for file in files_conservative:
    df = pd.read_csv(file)
    rewards_cons.append(df['exploration/Returns Mean'].values[:x_length_cons])
    epochs_cons.append(df['Epoch'].values[:x_length_cons])
rewards_cons = np.array(rewards_cons)
rewards_mean_cons = np.mean(rewards_cons,axis=0)
rewards_std_cons = np.std(rewards_cons,axis=0)

#get baseline average performance and std from runs owhen the conservative condition is verified
baseline_values = []
for file in files_conservative:
    df = pd.read_csv(file)
    baseline_values.extend(df[df['condition/Conservative_Condition_Respected']==False]['exploration/Returns Mean'])
baseline_mean = np.mean(baseline_values)*np.ones(x_length)
baseline_std = np.std(baseline_values)*np.ones(x_length)

#plot
#epochs[0][:x_length],
plt.figure(1)
plt.plot(rewards_mean[:x_length])
plt.fill_between(np.arange(x_length), rewards_mean[:x_length] + rewards_std[:x_length], rewards_mean[:x_length]-rewards_std[:x_length], alpha=0.2)
plt.plot(rewards_mean_cons[:x_length_cons])
plt.fill_between(np.arange(x_length_cons), rewards_mean_cons[:x_length_cons] + rewards_std_cons[:x_length_cons], rewards_mean_cons[:x_length_cons]-rewards_std_cons[:x_length_cons], alpha=0.2)
plt.plot(baseline_mean[:x_length], '--', color='tab:red')
plt.fill_between(np.arange(x_length), baseline_mean[:x_length] + baseline_std[:x_length], baseline_mean[:x_length]-baseline_std[:x_length], alpha=0.2, color='tab:red')

plt.title('Performance')
plt.legend(['SAC', 'Conservative SAC', 'baseline'])


##########Plot conservative condition###############

#get baseline cumulated performance:
cond_RHS = (1-alpha)*np.cumsum(baseline_mean)

#get no conservative run cumulative performance
cond_LHS_no_conservative = np.cumsum(rewards, axis=1)
cond_no_conservative = cond_LHS_no_conservative - cond_RHS
mean_cnc = np.mean(cond_no_conservative, axis = 0)
std_cnc = np.std(cond_no_conservative, axis = 0)

#get conservative run cumulative performance
cond_LHS_conservative = np.cumsum(rewards_cons, axis=1)
cond_conservative = cond_LHS_conservative - cond_RHS[:x_length_cons]
mean_cc = np.mean(cond_conservative, axis = 0)
std_cc = np.std(cond_conservative, axis = 0)

#plot
plt.figure(2)
plt.plot(mean_cnc)
plt.fill_between(np.arange(x_length), mean_cnc[:x_length] + std_cnc[:x_length], mean_cnc[:x_length]-std_cnc[:x_length], alpha=0.2)
plt.plot(mean_cc)
plt.fill_between(np.arange(x_length_cons), mean_cc[:x_length_cons] + std_cc[:x_length_cons], mean_cc[:x_length_cons]-std_cc[:x_length_cons], alpha=0.2)
plt.plot(np.zeros(x_length), '--', color='tab:red')

plt.title('Conservative Condition')
plt.legend(['SAC', 'Conservative SAC', 'baseline'])

plt.show()
