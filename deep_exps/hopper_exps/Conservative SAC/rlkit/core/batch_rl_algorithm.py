import abc
from collections import OrderedDict
import gtimer as gt
from rlkit.core.rl_algorithm import BaseRLAlgorithm
from rlkit.data_management.replay_buffer import ReplayBuffer
from rlkit.samplers.data_collector import PathCollector
from rlkit.core import logger, eval_util
import rlkit.torch.pytorch_util as ptu
import torch
import numpy as np
import copy

def _get_epoch_timings():
    times_itrs = gt.get_times().stamps.itrs
    times = OrderedDict()
    epoch_time = 0
    for key in sorted(times_itrs):
        time = times_itrs[key][-1]
        epoch_time += time
        times['time/{} (s)'.format(key)] = time
    times['time/epoch (s)'] = epoch_time
    times['time/total (s)'] = gt.get_times().total
    return times

class BatchRLAlgorithm(BaseRLAlgorithm, metaclass=abc.ABCMeta):
    def __init__(
            self,
            trainer,
            baseline,
            exploration_env,
            evaluation_env,
            exploration_data_collector: PathCollector,
            evaluation_data_collector: PathCollector,
            baseline_expl_data_collector: PathCollector,
            replay_buffer: ReplayBuffer,
            batch_size,
            max_path_length,
            num_epochs,
            num_eval_steps_per_epoch,
            num_expl_steps_per_train_loop,
            num_trains_per_train_loop,
            num_safe_trains_per_train_loop,
            num_ensemble = 5,
            alpha_ce = 0.8,
            lambda_ce = 1.0,
            num_train_loops_per_epoch=1,
            min_num_steps_before_training=0,
            save_frequency=0
    ):
        super().__init__(
            trainer,
            exploration_env,
            evaluation_env,
            exploration_data_collector,
            evaluation_data_collector,
            replay_buffer,
        )
        self.batch_size = batch_size
        self.max_path_length = max_path_length
        self.num_epochs = num_epochs
        self.num_eval_steps_per_epoch = num_eval_steps_per_epoch
        self.num_trains_per_train_loop = num_trains_per_train_loop
        self.num_train_loops_per_epoch = num_train_loops_per_epoch
        self.num_expl_steps_per_train_loop = num_expl_steps_per_train_loop
        self.min_num_steps_before_training = min_num_steps_before_training
        self.save_frequency = save_frequency
        self.baseline_expl_data_collector = baseline_expl_data_collector
        self.num_safe_trains_per_train_loop = num_safe_trains_per_train_loop
        self.baseline = baseline
        self.critic_history = []
        self.baseline_critic_history = []
        self.alpha_ce = alpha_ce
        self.lambda_ce = lambda_ce
        self.num_ensemble = num_ensemble

    def _train(self):

        #sample a first baseline trajectory
        if self.min_num_steps_before_training > 0:
            self.play_safe = True
            init_expl_paths = self.baseline_expl_data_collector.collect_new_paths(
                self.max_path_length,
                self.min_num_steps_before_training,
                discard_incomplete_paths=False,
            )
            self.replay_buffer.add_paths(init_expl_paths)
            self.baseline_expl_data_collector.end_epoch(-1)

        #iteration loop
        for epoch in gt.timed_for(
                range(self._start_epoch, self.num_epochs),
                save_itrs=True,
        ):
            self.eval_data_collector.collect_new_paths(
                self.max_path_length,
                self.num_eval_steps_per_epoch,
                discard_incomplete_paths=True,
            )
            gt.stamp('evaluation sampling')

            #evaluate the Q-value of the exploration policy using ensemble of approximators
            #ps: self.num_train_loops_per_epoch is set to 1
            for _ in range(self.num_train_loops_per_epoch):

                #check conservative condition using ensemble learning
                Qs_lst = self.shadow_batch_evaluation()
                self.Qs_mean = np.mean(Qs_lst)
                self.Qs_std = np.std(Qs_lst)
                self.Qs = np.mean(Qs_lst) - self.lambda_ce * np.std(Qs_lst)
                self.Qb = self._get_value(*self.baseline)
                self.baseline_critic_history.append(self.Qb)

                self.LHS = np.sum(self.critic_history) + self.Qs
                self.RHS = (1-self.alpha_ce)*np.sum(self.baseline_critic_history)
                print()
                print('LHS: ', self.LHS)
                print('RHS: ', self.RHS)

                #conservative condition
                self.play_safe = self.LHS <= self.RHS

                if self.LHS > self.RHS: #sample with learnes policy
                    print('Play Learned Policy')
                    self.critic_history.append(self.Qs)
                    new_expl_paths = self.expl_data_collector.collect_new_paths(
                        self.max_path_length,
                        self.num_expl_steps_per_train_loop,
                        discard_incomplete_paths=False,
                    )

                else: #sample with baseline
                    print('Play Baseline Policy')
                    self.critic_history.append(self.Qb)
                    new_expl_paths = self.baseline_expl_data_collector.collect_new_paths(
                        self.max_path_length,
                        self.num_expl_steps_per_train_loop,
                        discard_incomplete_paths=False,
                    )

                print()

                #store the sampled path into the buffer
                #Note: samplers are modified to sample only one episode see:rlkit/samplers/data_collector/path_collector.py
                gt.stamp('exploration sampling', unique=False)
                self.replay_buffer.add_paths(new_expl_paths)
                gt.stamp('data storing', unique=False)

                #Optimize policies and Q values
                self.training_mode(True)
                for _ in range(self.num_trains_per_train_loop):
                    train_data = self.replay_buffer.random_batch(
                        self.batch_size)
                    self.trainer.train(train_data)
                gt.stamp('training', unique=False)
                self.training_mode(False)

            self.end_epoch(epoch)

            if self.save_frequency > 0:
                if epoch % self.save_frequency == 0:
                    self.trainer.save_models(epoch)
                    print('model saved')


    #create N trainers to evaluate Q values uncertainty
    def shadow_batch_evaluation(self):
        from rlkit.torch.sac.sac import SACTrainer
        Qs_lst = []
        for _ in range(self.num_ensemble):
            #clone the trainer
            safe_trainer = copy.deepcopy(self.trainer)
            self.training_mode(True)
            for _ in range(self.num_safe_trains_per_train_loop):
                train_data = self.replay_buffer.random_batch(self.batch_size)
                safe_trainer.train(train_data)
            gt.stamp('training', unique=False)
            self.training_mode(False)

            Qs_lst.append(self._get_value(safe_trainer.policy, safe_trainer.qf1, safe_trainer.qf2))

        return Qs_lst

    # Retrieve Q value
    def _get_value(self, policy, critic1, critic2):

        obs = self.expl_env.reset()
        policy_action, _ = policy.get_action(obs)

        obs = ptu.from_numpy(obs).float()
        policy_action = ptu.from_numpy(policy_action).float()
        obs = obs.reshape(1,-1)
        policy_action = policy_action.reshape(1,-1)
        with torch.no_grad():
            target_Q1 = critic1(obs, policy_action)
            target_Q2 = critic2(obs, policy_action)

        return 0.5*(target_Q1 + target_Q2)

    #util function to store relevant infos to logs
    def get_diagnostics(self):
        stats = OrderedDict([
            ('conservative', 1),
            ('lower_bound_value', self.Qs),
            ('mean_value', self.Qs_mean),
            ('std_value', self.Qs_std),
            ('baseline_value', self.Qb),
            ('LHS', self.LHS),
            ('RHS', self.RHS),
            ('Conservative_Condition_Respected', not self.play_safe)
        ])
        return stats

    #util function to store relevant infos to logs
    def end_epoch(self, epoch):
        snapshot = self._get_snapshot()
        logger.save_itr_params(epoch, snapshot)
        gt.stamp('saving')
        self.log_stats(epoch)

        self.expl_data_collector.end_epoch(epoch)
        self.eval_data_collector.end_epoch(epoch)
        self.replay_buffer.end_epoch(epoch)
        self.trainer.end_epoch(epoch)

        for post_epoch_func in self.post_epoch_funcs:
            post_epoch_func(self, epoch)

    #util function to store relevant infos to logs
    def log_stats(self, epoch):
        logger.log("Epoch {} finished".format(epoch), with_timestamp=True)

        """
        Replay Buffer
        """
        logger.record_dict(
            self.replay_buffer.get_diagnostics(),
            prefix='replay_buffer/'
        )

        """
        Trainer
        """
        logger.record_dict(self.trainer.get_diagnostics(), prefix='trainer/')

        """
        Exploration
        """
        if self.play_safe:
            logger.record_dict(
                self.baseline_expl_data_collector.get_diagnostics(),
                prefix='exploration/'
            )
            expl_paths = self.baseline_expl_data_collector.get_epoch_paths()
        else:
            logger.record_dict(
                self.expl_data_collector.get_diagnostics(),
                prefix='exploration/'
            )
            expl_paths = self.expl_data_collector.get_epoch_paths()

        if hasattr(self.expl_env, 'get_diagnostics'):
            logger.record_dict(
                self.expl_env.get_diagnostics(expl_paths),
                prefix='exploration/',
            )
        logger.record_dict(
            eval_util.get_generic_path_information(expl_paths),
            prefix="exploration/",
        )
        """
        Evaluation
        """
        logger.record_dict(
            self.eval_data_collector.get_diagnostics(),
            prefix='evaluation/',
        )
        eval_paths = self.eval_data_collector.get_epoch_paths()
        if hasattr(self.eval_env, 'get_diagnostics'):
            logger.record_dict(
                self.eval_env.get_diagnostics(eval_paths),
                prefix='evaluation/',
            )
        logger.record_dict(
            eval_util.get_generic_path_information(eval_paths),
            prefix="evaluation/",
        )
        """
        Conservative condition
        """
        logger.record_dict(self.get_diagnostics(), prefix='condition/')
        """
        Misc
        """
        gt.stamp('logging')
        logger.record_dict(_get_epoch_timings())
        logger.record_tabular('Epoch', epoch)
        logger.dump_tabular(with_prefix=False, with_timestamp=False)
