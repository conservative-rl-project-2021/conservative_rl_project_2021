import os
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import pdb

path = '/home/mathias/Bureau/rlkit-master/data'
def findfiles(path, regex):
    res = []
    for root, dirs, fnames in os.walk(path):
        for fname in fnames:
            if regex in fname:
                res.append(os.path.join(root, fname))
    return res

files = findfiles(path, 'progress.csv')
print(files)
rewards, epochs = [], []
for file in files:
    df = pd.read_csv(file)
    rewards.append(df['evaluation/Returns Mean'].values)
    epochs.append(df['Epoch'].values)

array = np.array(rewards)
print(array.shape)

rewards_mean = np.mean(array,axis=0)
rewards_std = np.std(array,axis=0)
plt.figure(1)
plt.plot(epochs[0], rewards_mean)
plt.fill_between(epochs[0], rewards_mean + rewards_std, rewards_mean-rewards_std, alpha=0.2)
plt.title('performance')
plt.show()

plt.figure(2)
