# coding=utf-8
# Copyright 2021 The Google Research Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Utilities for generating and storing experience transition data.

Provides mechanisms for generating off-policy transition data given a behavior
policy as well as storing and sampling from this off-policy data.
"""

from __future__ import absolute_import
from __future__ import division

from __future__ import print_function

import abc

from collections import namedtuple

import attr
import numpy as np
import six
from typing import Any, Callable, Iterable, Optional, Sequence, Tuple

import dual_dice.policy as policy_lib



@attr.s
class OPETransitionTuple(object):
    state = attr.ib()
    action = attr.ib()
    reward = attr.ib()
    next_state = attr.ib()
    discount = attr.ib()
    initial_state = attr.ib()
    time_step = attr.ib()
    action_sampled_prob = attr.ib()


@six.add_metaclass(abc.ABCMeta)
class OPETransitionData(object):
    """Abstract class representing a store of off-policy data."""

    @abc.abstractmethod
    def sample_batch(self, batch_size):
        """Samples batch of sarsa data."""

    @abc.abstractmethod
    def get_all(self):
        """Gets all stored transition data."""

    @abc.abstractmethod
    def iterate_once(self):
        """Iterate over all data once."""

    @property
    @abc.abstractmethod
    def policy(self):
        """Get policy used to sample off-policy data, if it exists."""


class OPETrajectoryData(OPETransitionData):
    
    """Transition data storage based on contiguous trajectories.
    args:
        Policy: If the policy gathering the data is known. It helps only for IS techniques.
    """
    
    def __init__(self):
        self._trajectories = []
        self._num_data = 0
        self.data = OPETransitionTuple([], [], [], [], [], [], [], [])
    
    def push(self, trajectory, policy=None):
        """Save an entire trajectory"""
            
        # Add trajectory to total
        self._trajectories.append(trajectory)
            
        initial_state, _, _, _ = trajectory[0]
        for step, (state, action, reward, next_state) in enumerate(trajectory):
            self.data.state.append(state)
            self.data.action.append(action)
            self.data.reward.append(reward)
            self.data.next_state.append(next_state)

            # Discount is always 1 (infinite-horizon setting).
            self.data.discount.append(0.99**step)

            self.data.initial_state.append(initial_state)
            self.data.time_step.append(step)
            action_sampled_prob = (None if policy is None else
                               policy.get_probability(state, action))
            self.data.action_sampled_prob.append(action_sampled_prob)

            self._num_data += 1

        # Convert data to NumPy array.
        self._data = OPETransitionTuple(*map(np.array, attr.astuple(self.data)))


    def sample_batch(self, batch_size):
        """Samples batch of sarsa data."""
        indices = np.random.choice(self._num_data, batch_size, replace=True)
        return OPETransitionTuple(
            *[np.take(arr, indices, axis=0) for arr in attr.astuple(self._data)])

    def get_all(self):
        """Gets all stored sarsa data."""
        return self._data

    def iterate_once(self):
        """Iterates over all data once."""
        for idx in range(self._num_data):
            yield OPETransitionTuple(
                *[np.take(arr, idx, axis=0) for arr in attr.astuple(self._data)])

    @property
    def policy(self):
        """Gets policy used to sample off-policy data, if it exists."""
        return self._policy

    @property
    def trajectories(self):
        """Gets original trajectories used to create off-policy data."""
        return self._trajectories

