import numpy as np
import random


def collect_data(env, policy, eps, dqn_buffer, ope_buffer,
                 num_trajectories=1, trajectory_length=200, 
                 gamma=0.99, reward_fn=None, init=False, save_policy=False):
    """Creates off-policy dataset by running a behavior policy in an environment.

    Args:
    env: An environment.
    policy: A behavior policy.
    eps: Epsilon for DQN policy
    dqn_buffer: DQN Replay Buffer
    ope_buffer: Replay Buffer for OPE
    num_trajectories: Number of trajectories to collect.
    trajectory_length: Desired length of each trajectory; how many steps to run
      behavior policy in the environment before resetting.
    gamma: Discount used for total and average reward calculation.
    reward_fn: A function (default None) in case the environment reward
      should be overwritten. This function should take in the environment
      reward and the environment's `done' flag and should return a new reward
      to use. A new reward function must be passed in for environments that
      terminate, since the code assumes an infinite-horizon setting.
    init: At the beginning, we fill the OPEBuffer with many trajectories, and not the DQN Buffer

    Returns:
    dqn_buffer: 
    ope_buffer: 
    avg_episode_rewards: Compute per-episode discounted rewards averaged over
      the trajectories.
    avg_step_rewards: Computed per-step average discounted rewards averaged
      over the trajectories.

    Raises:
    ValueError: If the environment terminates and a reward_fn is not passed in.
    """
    trajectories = []
    trajectory_rewards = []
    total_mass = 0  # For computing average per-step reward.
    for _ in range(num_trajectories):
        trajectory = []
        total_reward = 0
        discount = 1
        state = env.reset()
        for _ in range(trajectory_length):
            # action = policy.sample_action(state)
            action = policy.act_e_greedy(state, epsilon=eps)
            next_state, reward, done, _ = env.step(action)
            if reward_fn is not None:
                reward = reward_fn(reward, done)
            elif done:
                raise ValueError(
                'Environment terminated but reward_fn is not specified.')

            trajectory.append((state, action, reward, next_state))
            
            # Add DQN Buffer if not init
            if not init:
                dqn_buffer.append((state, action, reward, next_state, done))
                # dqn_buffer.push(state, action, next_state, reward)
            
            total_reward += reward * discount
            total_mass += discount

            state = next_state
            discount *= gamma

        # Add OPE Buffer 
        if save_policy:
            ope_buffer.push(trajectory,  policy=policy)
        else:
            ope_buffer.push(trajectory)
        
        # Logs
        trajectory_rewards.append(total_reward)
        avg_step_rewards = np.sum(trajectory_rewards) / total_mass

    avg_episode_rewards = np.mean(trajectory_rewards)
    avg_step_rewards = np.sum(trajectory_rewards) / total_mass

    return (dqn_buffer, ope_buffer, avg_episode_rewards, avg_step_rewards)


