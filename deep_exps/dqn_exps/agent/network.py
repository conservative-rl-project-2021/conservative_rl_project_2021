import torch
import torch.nn as nn
import torch.optim as optim

class DQN(nn.Module):

    def __init__(self, env):
        
        super(DQN, self).__init__()
        
        # Dimensions
        self.env = env
        self.input_dim = self.env.observation_space.shape[0]
        self.output_dim = self.env.action_space.n
        self.hidden = 64
        
        self.features = nn.Sequential(
            nn.Linear(self.input_dim, self.hidden),
            nn.ReLU(),
            nn.Linear(self.hidden, self.hidden),
            nn.ReLU())
        self.adv = nn.Linear(self.hidden, self.output_dim)
        self.val = nn.Linear(self.hidden, 1)

    def forward(self, x):
        x = self.features(x)
        adv = self.adv(x)
        val = self.val(x)
        return val + adv - adv.mean(1, keepdim=True)