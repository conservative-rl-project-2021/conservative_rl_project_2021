import torch
import torch.nn as nn
import torch.optim as optim

import numpy as np
import random

from agent.network import DQN


class Agent:

    def __init__(self, env):
        
        # Global information
        self.batch_size = 32
        self.gamma = 0.99
        self.env = env
        self.output_dim = self.env.action_space.n
        
        # Network
        self.online_net = DQN(self.env)
        self.online_net.train()
        self.target_net = DQN(self.env)
        self.update_target_net()
        for param in self.target_net.parameters(): param.requires_grad = False

        # Optim
        self.lr = 1e-3
        self.optimiser = optim.Adam(self.online_net.parameters(), lr=self.lr)
        
        self.action_space = self.online_net.output_dim
        self.epsilon = 1

    def act(self, state):
        state = torch.FloatTensor(state).unsqueeze(0)
        with torch.no_grad():
            return self.online_net(state).argmax(1).item()
        
    def sample_action(self, state):
        return self.act_e_greedy(state, self.epsilon, batch=True)

    def act_e_greedy2(self, state, epsilon=0.01):
        if random.random() < epsilon:
            action = random.randrange(self.output_dim)
            if action != self.act(state):
                return action, epsilon/(self.action_space)
            else:
                return action, 1 - epsilon + epsilon/self.action_space
        else:
            return self.act(state), 1 - epsilon + epsilon/self.action_space
  
    def act_e_greedy(self, state, epsilon=0.01, batch=False):
        if batch:
            actions = np.zeros(state.shape[0])
            for i, elt in enumerate(state):
                if random.random() < epsilon:
                    actions[i] = random.randrange(self.output_dim)
                else:
                    actions[i] = self.act(elt)
            return actions
        else:
            if random.random() < epsilon:
                return random.randrange(self.output_dim)
            else:
                return self.act(state)

    def _sample_batch(self, buffer):
        s, a, r, ns, t = zip(*random.sample(buffer, self.batch_size))
        f, l = torch.FloatTensor, torch.LongTensor
        return f(s), l(a), f(r), f(ns), f(t)
    
    def get_probability(self, state, action):
        probs = np.zeros(state.shape[0])
        with torch.no_grad():
            state = torch.FloatTensor(state)
            next_actions = self.online_net(state).max(1)[1].numpy()
        for i, act in enumerate(action):
            if act == next_actions[i]:
                probs[i] = 1 - self.epsilon + self.epsilon/self.action_space
            else:
                probs[i] = self.epsilon/self.action_space
        return probs

    def get_probabilities(self, state):
        probs = np.zeros((state.shape[0], self.action_space))
        with torch.no_grad():
            state = torch.FloatTensor(state)
            next_actions = self.online_net(state).max(1)[1].numpy()
        for i, act in enumerate(next_actions):
            for j in range(self.action_space):
                if act == j:
                    probs[i, j] = 1 - self.epsilon + self.epsilon/self.action_space
                else:
                    probs[i, j] = self.epsilon/self.action_space
        return probs

    def train_iter(self, buffer):
        state, action, reward, next_state, terminal = self._sample_batch(buffer)

        q_value = self.online_net(state)[range(self.batch_size), action]
        with torch.no_grad():
            next_state_action = self.online_net(next_state).max(1)[1] # online_net to get action for next_state
            next_qv = self.target_net(next_state)[range(self.batch_size), next_state_action] # target_net to get Q
            target_qv = reward + self.gamma * (1 - terminal) * next_qv

        loss = (q_value - target_qv).pow(2).mean()

        self.optimiser.zero_grad()
        loss.backward()
        self.optimiser.step()
        return loss.item()

    def update_target_net(self):
        self.target_net.load_state_dict(self.online_net.state_dict())
        
    def save_network(self, save_path):
        torch.save(self.online_net.state_dict(), save_path)
    
    def load_network(self, load_path):
        self.online_net.load_state_dict(torch.load(load_path))
    
        
        