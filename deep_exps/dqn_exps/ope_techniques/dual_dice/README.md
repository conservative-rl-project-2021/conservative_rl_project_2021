# DualDICE

Code for DualDICE as described in `DualDICE: Behavior-Agnostic Estimation of
Discounted Stationary Distribution Corrections' by Ofir Nachum, Yinlam Chow, Bo
Dai, Lihong Li.`


## Help

For now, it has not been tested yet so there should be some small mistakes.

### Buffers

In `memories`:
- `ope_replay_buffer`: Buffer to use for OPE. **Very important to use this Buffer for DICE to work.**
- `dqn_buffer`: Proposal pour DQN Replay Buffer. I find it very clean, but we can use anything.
- `collect_data`: Function to collect data and put it in the required buffers. **To plug in DQN code.**

### Estimate value function
- In`dice_estimate_from_dataset.py`, use `dice_estimation` to estimate value function

## Additional Information

When tuning DualDICE, some things to keep in mind:
*   Optimization of nu and zeta at the same time can be unstable. Try utilizing
    the --deterministic_env flag, which trains nu on its own (without
    application of Fenchel duality).
*   Lower gamma can make optimization easier.
*   Otherwise, nu and zeta learning rate have the biggest impact on performance.

When evaluating DualDICE, keep in mind that the print-out of the Target (oracle)
rewards is an estimate! In a perfect world this would be calculated with
num_trajectories=\infty and max_trajectory_length=\infty. In practice, to get a
better estimate, just set these to a large value (e.g., 1000).

This is a minimal version of the code. For example, continuous-action
environments are not supported.
