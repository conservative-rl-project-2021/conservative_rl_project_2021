

def get_policy_ratio(policy0, policy1, state, action,
                     epsilon = 0.0):
    """Compute the ratio of two policies sampling an action at a state.
    Args:
    policy0: The first (baseline) policy object.
    policy1: The second (target) policy object.
    state: The state.
    action: The action.
    epsilon: An optional small constant to add to both the numerator and
      denominator.  Defaults to 0.
    Returns:
    The ratio of probability of the second policy selecting the action compared
      to the probability of the first policy selecting the action.
    """
    probability0 = policy0.get_probability(state, action)
    probability1 = policy1.get_probability(state, action)
    return (probability1 + epsilon) / (probability0 + epsilon)