# coding=utf-8
# Copyright 2021 The Google Research Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Run off-policy policy evaluation."""

from __future__ import absolute_import
from __future__ import division

from __future__ import print_function

import os

from absl import app
from absl import flags
import numpy as np
import tensorflow.compat.v1 as tf

import ope_techniques.dual_dice.algos.dual_dice as dual_dice
import ope_techniques.dual_dice.algos.neural_dual_dice as neural_dual_dice
# import ope_techniques.dual_dice.gridworld.environments as gridworld_envs
# import ope_techniques.dual_dice.gridworld.policies as gridworld_policies
# import ope_techniques.dual_dice.transition_data as transition_data

# Solver args
FLAGS = flags.FLAGS
flags.DEFINE_float('function_exponent', 1.5,
                   'Exponent for f function in DualDICE.')
flags.DEFINE_integer('batch_size', 512,
                     'batch_size for training models.')
flags.DEFINE_integer('num_steps', 10000,
                     'num_steps for training models.')
flags.DEFINE_integer('log_every', 50, 'log after certain number of steps.')
flags.DEFINE_float('nu_learning_rate', 0.0001, 'nu lr')
flags.DEFINE_float('zeta_learning_rate', 0.001, 'z lr')

# Hyperparams
function_exponent = 1.5
batch_size = 512
num_steps = 20000
log_every = 50
nu_learning_rate = 0.0001
zeta_learning_rate = 0.001


def get_solver(env, gamma, summary_writer):
    """Create solver object."""
    state_dim = env.observation_space.shape[0]
    action_dim = env.action_space.n
    neural_solver_params = neural_dual_dice.NeuralSolverParameters(
        state_dim,
        action_dim,
        gamma,
        discrete_actions=True,
        deterministic_env=True,
        nu_learning_rate=nu_learning_rate,
        zeta_learning_rate=zeta_learning_rate,
        batch_size=batch_size,
        num_steps=num_steps,
        log_every=log_every,
        summary_writer=summary_writer,
        summary_prefix='')

    return neural_dual_dice.NeuralDualDice(
          parameters=neural_solver_params,
          function_exponent=function_exponent)


def dice_estimation(env, gamma, behavior_data, policy, baseline_policy):
    
    # Get summary writer
    save_dir = './dice_estimation/'
    hparam_format = ('{N_LR}_{Z_LR}_{GAM}_{SOLVER}')
    hparam_str = hparam_format.format(
        GAM=gamma,
        N_LR=nu_learning_rate,
        Z_LR=zeta_learning_rate,
        SOLVER='DICE')
    
    summary_dir = os.path.join(save_dir, hparam_str)
    summary_writer = tf.summary.FileWriter(summary_dir)

    # Get solver
    density_estimator = get_solver(env, gamma, summary_writer)
    
    # Solve for estimated density ratios.
    est_avg_rewards = density_estimator.solve(behavior_data, policy, baseline_policy)
    
    # Close estimator properly.
    density_estimator.close()
    
    print('Estimated DualDICE average step reward:', est_avg_rewards)
    
    return est_avg_rewards


def main(argv):
    del argv
    start_seed = FLAGS.seed
    num_seeds = FLAGS.num_seeds
    num_trajectories = FLAGS.num_trajectories
    max_trajectory_length = FLAGS.max_trajectory_length
    alpha = FLAGS.alpha
    gamma = FLAGS.gamma
    nu_learning_rate = FLAGS.nu_learning_rate
    zeta_learning_rate = FLAGS.zeta_learning_rate
    tabular_obs = FLAGS.tabular_obs
    tabular_solver = FLAGS.tabular_solver
    if tabular_solver and not tabular_obs:
        raise ValueError('Tabular solver can only be used with tabular obs.')
    env_name = FLAGS.env_name
    solver_name = FLAGS.solver_name
    save_dir = FLAGS.save_dir

    hparam_format = ('{ENV}_{ALPHA}_{NUM_TRAJ}_{TRAJ_LEN}_'
                   '{N_LR}_{Z_LR}_{GAM}_{SOLVER}')
    solver_str = (solver_name + tabular_solver * '-tab' +
                '-%.1f' % FLAGS.function_exponent)
    hparam_str = hparam_format.format(
      ENV=env_name + tabular_obs * '-tab',
      ALPHA=alpha,
      NUM_TRAJ=num_trajectories,
      TRAJ_LEN=max_trajectory_length,
      GAM=gamma,
      N_LR=nu_learning_rate,
      Z_LR=zeta_learning_rate,
      SOLVER=solver_str)

    if save_dir:
        summary_dir = os.path.join(save_dir, hparam_str)
        if num_seeds == 1:
            summary_dir = os.path.join(summary_dir, 'seed%d' % start_seed)
        summary_writer = tf.summary.FileWriter(summary_dir)
    else:
        summary_writer = None

    env, policy0, policy1 = get_env_and_policies(env_name, tabular_obs, alpha)

    results = []
    for seed in range(start_seed, start_seed + num_seeds):
        print('Seed', seed)
        if num_seeds == 1:
            summary_prefix = ''
        else:
            summary_prefix = 'seed%d/' % seed
        np.random.seed(seed)
        # Off-policy data.
        (behavior_data, behavior_avg_episode_rewards,
         behavior_avg_step_rewards) = transition_data.collect_data(
             env,
             policy0,
             num_trajectories,
             max_trajectory_length,
             gamma=gamma)
        print('Behavior average episode rewards', behavior_avg_episode_rewards)
        print('Behavior average step rewards', behavior_avg_step_rewards)
        # Oracle on-policy data.
        (target_data, target_avg_episode_rewards,
         target_avg_step_rewards) = transition_data.collect_data(
             env,
             policy1,
             num_trajectories,
             max_trajectory_length,
             gamma=gamma)
        print('Target (oracle) average episode rewards', target_avg_episode_rewards)
        print('Target (oracle) average step rewards', target_avg_step_rewards)

        if tabular_obs:
            behavior_state_frequency = count_state_frequency(behavior_data,
                                                           env.num_states, gamma)
            target_state_frequency = count_state_frequency(target_data,
                                                         env.num_states, gamma)
            empirical_density_ratio = (
                target_state_frequency / (1e-8 + behavior_state_frequency))
            print('Empirical state density ratio', empirical_density_ratio[:4], '...')
        del target_data  # Don't use oracle in later code.

        # Get solver.
        density_estimator = get_solver(solver_name, env, gamma, tabular_solver,
                                       summary_writer, summary_prefix)
        # Solve for estimated density ratios.
        est_avg_rewards = density_estimator.solve(behavior_data, policy1)
        # Close estimator properly.
        density_estimator.close()
        print('Estimated (solver: %s) average step reward' % solver_name,
              est_avg_rewards)
        results.append(
            [behavior_avg_step_rewards, target_avg_step_rewards, est_avg_rewards])

    if save_dir is not None:
        filename = os.path.join(save_dir, '%s.npy' % hparam_str)
        print('Saving results to %s' % filename)
        if not tf.gfile.IsDirectory(save_dir):
            tf.gfile.MkDir(save_dir)
        with tf.gfile.GFile(filename, 'w') as f:
            np.save(f, np.array(results))
        print('Done!')

