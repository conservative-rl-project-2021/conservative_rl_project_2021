import argparse
import torch
import rlkit.torch.pytorch_util as ptu

from rlkit.data_management.env_replay_buffer import EnsembleEnvReplayBuffer
from rlkit.envs.wrappers import NormalizedBoxEnv
from rlkit.launchers.launcher_util import setup_logger_custom, set_seed
from rlkit.samplers.data_collector import EnsembleMdpOnePathCollector
from rlkit.torch.sac.policies import TanhGaussianPolicy, MakeDeterministic
from rlkit.torch.sac.neurips21_sac_conservative import NeurIPS21SACConservativeEnsembleTrainer
from rlkit.torch.networks import FlattenMlp
from rlkit.torch.torch_rl_algorithm import TorchConservativeBatchRLAlgorithm

# Global for Conservative Exploration
BASELINE_NUM_ENSEMBLE = 5
path = 'data/gym_cheetah/Conservative-SUNRISE/Conservative_SUNRISE_cons_False_alpha_ce_0.20_en_5_batch_256_layer_2_seed_1234/model'
ratio = 5
step = 27

def parse_args():
    parser = argparse.ArgumentParser()
    # architecture
    parser.add_argument('--num_layer', default=2, type=int)
    
    # train
    parser.add_argument('--batch_size', default=256, type=int)
    parser.add_argument('--save_freq', default=0, type=int)

    # misc
    parser.add_argument('--seed', default=1, type=int)
    
    # env
    parser.add_argument('--env', default="halfcheetah_poplin", type=str)
    
    # ensemble
    parser.add_argument('--num_ensemble', default=3, type=int)
    parser.add_argument('--ber_mean', default=0.5, type=float)
    
    # inference
    parser.add_argument('--inference_type', default=0.0, type=float)
    
    # corrective feedback
    parser.add_argument('--temperature', default=20.0, type=float)
    
    # conservative or not
    parser.add_argument('--conservative', action='store_true', default=False)
    
    # alpha in conservative condition (between 0 and 1)
    parser.add_argument('--alpha_ce', default=0.2, type=float)
    
    args = parser.parse_args()
    return args

def load_models(path, step, num_ensemble, policy, qf1, qf2):
    policy.load_state_dict(
         torch.load('%s/%d_th_actor_%s.pt' % (path, 0, step))
    )
    for en_index in range(num_ensemble):
        # policy[en_index].load_state_dict(
        #     torch.load('%s/%d_th_actor_%s.pt' % (path, en_index, step))
        # )
        qf1[en_index].load_state_dict(
            torch.load('%s/%d_th_1st_critic_%s.pt' % (path, en_index, step))
        )
        qf2[en_index].load_state_dict(
            torch.load('%s/%d_th_2nd_critic_%s.pt' % (path, en_index, step))
        )

def get_env(env_name, seed):
    if env_name == 'gym_cheetah':
        from gym.envs.mujoco import HalfCheetahEnv
        env = HalfCheetahEnv()
        env.seed(seed)
    elif env_name == 'gym_hopper':
        from gym.envs.mujoco import HopperEnv
        env = HopperEnv()
        env.seed(seed)
    elif env_name == 'gym_humanoid_standup':
        from gym.envs.mujoco import HumanoidStandupEnv
        env = HumanoidStandupEnv()
        env.seed(seed)
    elif env_name == 'gym_humanoid':
        from gym.envs.mujoco import HumanoidEnv
        env = HumanoidEnv()
        env.seed(seed)
    elif env_name == 'gym_ant':
        from gym.envs.mujoco import AntEnv
        env = AntEnv()
        env.seed(seed)    
        
    return env

def experiment(variant):
    expl_env = NormalizedBoxEnv(get_env(variant['env'], variant['seed']))
    eval_env = NormalizedBoxEnv(get_env(variant['env'], variant['seed']))
    obs_dim = expl_env.observation_space.low.size
    action_dim = eval_env.action_space.low.size
    
    M = variant['layer_size']
    num_layer = variant['num_layer']
    network_structure = [M] * num_layer
    
    NUM_ENSEMBLE = variant['num_ensemble']
    
    ######################################################################
    # CAREFUL: only one policy for N Q-estimates
    ######################################################################
    L_policy = TanhGaussianPolicy(
            obs_dim=obs_dim,
            action_dim=action_dim,
            hidden_sizes=network_structure,
        )
    # Since we only use the evaluation sampler (to respect the structure of CE)
    # We need to make it random
    # eval_policy = MakeDeterministic(policy) 
    L_eval_policy = L_policy
    L_qf1, L_qf2, L_target_qf1, L_target_qf2 = [], [], [], []
    
    
    
    for _ in range(NUM_ENSEMBLE):
    
        qf1 = FlattenMlp(
            input_size=obs_dim + action_dim,
            output_size=1,
            hidden_sizes=network_structure,
        )
        qf2 = FlattenMlp(
            input_size=obs_dim + action_dim,
            output_size=1,
            hidden_sizes=network_structure,
        )
        target_qf1 = FlattenMlp(
            input_size=obs_dim + action_dim,
            output_size=1,
            hidden_sizes=network_structure,
        )
        target_qf2 = FlattenMlp(
            input_size=obs_dim + action_dim,
            output_size=1,
            hidden_sizes=network_structure,
        )
        
        L_qf1.append(qf1)
        L_qf2.append(qf2)
        L_target_qf1.append(target_qf1)
        L_target_qf2.append(target_qf2)
        # L_policy.append(policy)
        # L_eval_policy.append(eval_policy)
        
    
    # Log baseline policy if necessary else set to None
    if variant['conservative']:
        L_baseline_policy = TanhGaussianPolicy(
                obs_dim=obs_dim,
                action_dim=action_dim,
                hidden_sizes=network_structure,
            )
        L_baseline_qf1, L_baseline_qf2 = [], []
    
        for _ in range(BASELINE_NUM_ENSEMBLE):

            baseline_qf1 = FlattenMlp(
                input_size=obs_dim + action_dim,
                output_size=1,
                hidden_sizes=network_structure,
            )
            baseline_qf2 = FlattenMlp(
                input_size=obs_dim + action_dim,
                output_size=1,
                hidden_sizes=network_structure,
            )

            L_baseline_qf1.append(baseline_qf1)
            L_baseline_qf2.append(baseline_qf2)
            # L_baseline_policy.append(baseline_policy)
        # Load models
        load_models(path, step, BASELINE_NUM_ENSEMBLE, L_baseline_policy, L_baseline_qf1, L_baseline_qf2)    
        
    else:
        L_baseline_policy = None
        L_baseline_qf1 = None
        L_baseline_qf2 = None
    
    eval_path_collector = EnsembleMdpOnePathCollector(
        eval_env,
        L_eval_policy,
        L_baseline_policy,
        NUM_ENSEMBLE,
        BASELINE_NUM_ENSEMBLE,
        ber_mean=variant['ber_mean'],
        critic1=L_qf1,
        critic2=L_qf2,
        baseline_critic1=L_baseline_qf1,
        baseline_critic2=L_baseline_qf2,
        inference_type=variant['inference_type'],
        feedback_type=1,
        eval_flag=True,
    )
    
    expl_path_collector = EnsembleMdpOnePathCollector(
        expl_env,
        L_policy,
        L_baseline_policy,
        NUM_ENSEMBLE,
        BASELINE_NUM_ENSEMBLE,
        ber_mean=variant['ber_mean'],
        eval_flag=False,
        critic1=L_qf1,
        critic2=L_qf2,
        baseline_critic1=L_baseline_qf1,
        baseline_critic2=L_baseline_qf2,
        inference_type=variant['inference_type'],
        feedback_type=1,
    )
    
    replay_buffer = EnsembleEnvReplayBuffer(
        variant['replay_buffer_size'],
        expl_env,
        NUM_ENSEMBLE,
        log_dir=variant['log_dir'],
    )
    
    trainer = NeurIPS21SACConservativeEnsembleTrainer(
        env=eval_env,
        policy=L_policy,
        qf1=L_qf1,
        qf2=L_qf2,
        target_qf1=L_target_qf1,
        target_qf2=L_target_qf2,
        num_ensemble=NUM_ENSEMBLE,
        feedback_type=1,
        temperature=variant['temperature'],
        temperature_act=0,
        expl_gamma=0,
        log_dir=variant['log_dir'],
        **variant['trainer_kwargs']
    )
    algorithm = TorchConservativeBatchRLAlgorithm(
        trainer=trainer,
        exploration_env=expl_env,
        evaluation_env=eval_env,
        exploration_data_collector=expl_path_collector,
        evaluation_data_collector=eval_path_collector,
        replay_buffer=replay_buffer,
        **variant['algorithm_kwargs']
    )
    
    algorithm.to(ptu.device)
    algorithm.train()


if __name__ == "__main__":
    args = parse_args()
    
    # noinspection PyTypeChecker
    variant = dict(
        algorithm="SAC_2",
        version="normal",
        layer_size=256,
        replay_buffer_size=int(1E6),
        algorithm_kwargs=dict(
            num_epochs=200,
            num_eval_steps_per_epoch=1000,
            num_trains_per_train_loop=100,
            num_expl_steps_per_train_loop=1000,
            min_num_steps_before_training=2000,
            max_path_length=1000,
            batch_size=args.batch_size,
            save_frequency=args.save_freq,
            conservative=args.conservative,
            alpha_ce=args.alpha_ce/ratio
        ),
        trainer_kwargs=dict(
            discount=0.99,
            soft_target_tau=5e-3,
            target_update_period=1,
            policy_lr=3E-4,
            qf_lr=3E-4,
            reward_scale=1,
            use_automatic_entropy_tuning=True,
        ),
        num_ensemble=args.num_ensemble,
        num_layer=args.num_layer,
        seed=args.seed,
        ber_mean=args.ber_mean,
        env=args.env,
        inference_type=args.inference_type,
        temperature=args.temperature,
        log_dir="",
        conservative=args.conservative,
        alpha_ce=args.alpha_ce
    )
                            
    set_seed(args.seed)
    exp_name = 'Conservative_SUNRISE'
    log_dir = setup_logger_custom(exp_name, variant=variant)
            
    variant['log_dir'] = log_dir
    # ptu.set_gpu_mode(True)
    experiment(variant)