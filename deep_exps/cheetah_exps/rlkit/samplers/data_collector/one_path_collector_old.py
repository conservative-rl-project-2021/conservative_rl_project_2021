from collections import deque, OrderedDict

from rlkit.core.eval_util import create_stats_ordered_dict
from rlkit.samplers.rollout_functions import rollout, multitask_rollout, ensemble_rollout, ensemble_eval_rollout, retrieve_init_values
from rlkit.samplers.rollout_functions import ensemble_ucb_rollout
from rlkit.samplers.data_collector.base import PathCollector

    
class EnsembleMdpOnePathCollector(PathCollector):
    """
    Collects one episode and put it in the replay buffer
    
    MODIFICATIONS
    
    Here we collect only one trajectory. To respect the structure of the code, one epoch is one episode.
    
    Careful: here the baseline policy is an EnsemblePolicy!!
    
    Here are the modifications for safety constaints: 
        - one new class agument: 
            - baseline_policy: to collect paths with baseline policy
        - additional arg to collect_new_paths:
            - safe (bool): collect paths with baseline policy or not
            - init (bool): if init, then we fill the memory. We fill it until a certain amount of data is stored.
            - seed (int): seed of the environment to make sure the initial state remains the same
    """
    
    def __init__(
            self,
            env,
            policy,
            baseline_policy,
            num_ensemble,
            baseline_num_ensemble,
            noise_flag=0,
            ber_mean=0.5,
            eval_flag=False,
            max_num_epoch_paths_saved=None,
            render=False,
            render_kwargs=None,
            critic1=None,
            critic2=None,
            baseline_critic1=None,
            baseline_critic2=None,
            inference_type=0.0,
            feedback_type=1,
    ):
        if render_kwargs is None:
            render_kwargs = {}
        self._env = env
        self._policy = policy
        self._baseline_policy = baseline_policy
        self._max_num_epoch_paths_saved = max_num_epoch_paths_saved
        self._epoch_paths = deque(maxlen=self._max_num_epoch_paths_saved)
        self._render = render
        self._render_kwargs = render_kwargs
        self.num_ensemble = num_ensemble
        self.baseline_num_ensemble = baseline_num_ensemble
        self.eval_flag = eval_flag
        self.ber_mean = ber_mean
        self.critic1 = critic1
        self.critic2 = critic2
        self.baseline_critic1 = baseline_critic1
        self.baseline_critic2 = baseline_critic2
        self.inference_type = inference_type
        self.feedback_type = feedback_type
        self._noise_flag = noise_flag
        
        self._num_steps_total = 0
        self._num_paths_total = 0
        
    
    def init_values(self, seed):
        lower_policy_value, baseline_value = retrieve_init_values(self._env, 
                                                                  self._policy, 
                                                                  self._baseline_policy, 
                                                                  self.critic1,
                                                                  self.critic2,
                                                                  self.baseline_critic1,
                                                                  self.baseline_critic2,
                                                                  self.num_ensemble,
                                                                  self.baseline_num_ensemble,
                                                                  self.inference_type,
                                                                  self.feedback_type,
                                                                  seed=seed)
        
        return lower_policy_value, baseline_value
    

    def collect_new_paths(
            self,
            max_path_length,
            num_steps,
            discard_incomplete_paths,
            seed=None,
            safe=False, 
            init=False,
    ):
        """
        Collect only trajectory instead of many.
        CAREFULL: the baseline policy must be an ensemble method here.
        """
        paths = []
        num_steps_collected = 0
        
        # If init: we collect num_steps transitions!!
        if init:
            while num_steps_collected < num_steps:
                max_path_length_this_loop = min(  # Do not go over num_steps
                    max_path_length,
                    num_steps - num_steps_collected,
                )
                if safe:
                    path = ensemble_rollout(
                        self._env,
                        self._policy,
                        self.num_ensemble,
                        noise_flag=self._noise_flag,
                        max_path_length=max_path_length,
                        ber_mean=self.ber_mean,
                        seed=seed
                    )
                else:
                    path = ensemble_rollout(
                        self._env,
                        self._policy,
                        self.num_ensemble,
                        noise_flag=self._noise_flag,
                        max_path_length=max_path_length_this_loop,
                        ber_mean=self.ber_mean,
                    )
                path_len = len(path['actions'])
                if (
                        path_len != max_path_length
                        and not path['terminals'][-1]
                        and discard_incomplete_paths
                ):
                    break
                num_steps_collected += path_len
                paths.append(path)
                
        
        # Collect one path if we are not initializing the memory 
        if safe:
            path = ensemble_rollout(
                self._env,
                self._baseline_policy,
                self.num_ensemble,
                noise_flag=self._noise_flag,
                max_path_length=max_path_length,
                ber_mean=self.ber_mean,
                seed=seed
            )
            num_steps_collected = len(path['actions'])
            paths.append(path)
        else:
            path = ensemble_rollout(
                        self._env,
                        self._policy,
                        self.num_ensemble,
                        noise_flag=self._noise_flag,
                        max_path_length=max_path_length,
                        ber_mean=self.ber_mean,
                        seed=seed
                    )

            # Add to list to be consistent with the rest of the code
            paths.append(path)
            num_steps_collected = len(path['actions'])
        
        # Update parameters
        self._num_paths_total += len(paths)
        self._num_steps_total += num_steps_collected
        self._epoch_paths.extend(paths)
        
        return paths

    def get_epoch_paths(self):
        return self._epoch_paths

    def end_epoch(self, epoch):
        self._epoch_paths = deque(maxlen=self._max_num_epoch_paths_saved)

    def get_diagnostics(self):
        path_lens = [len(path['actions']) for path in self._epoch_paths]
        stats = OrderedDict([
            ('num steps total', self._num_steps_total),
            ('num paths total', self._num_paths_total),
        ])
        stats.update(create_stats_ordered_dict(
            "path length",
            path_lens,
            always_show_all_stats=True,
        ))
        return stats

    def get_snapshot(self):
        return dict(
            env=self._env,
            policy=self._policy,
        )
