import abc
import random
import numpy as np

from rlkit.core.rl_algorithm import _get_epoch_timings
from rlkit.core import logger, eval_util
import gtimer as gt
from collections import OrderedDict
from rlkit.core.rl_algorithm import BaseRLAlgorithm
from rlkit.data_management.replay_buffer import ReplayBuffer
from rlkit.samplers.data_collector import PathCollector


class EpisodicConservativeBatchRLAlgorithm(BaseRLAlgorithm, metaclass=abc.ABCMeta):
    
    def __init__(
            self,
            trainer,
            exploration_env,
            evaluation_env,
            exploration_data_collector: PathCollector,
            evaluation_data_collector: PathCollector,
            replay_buffer: ReplayBuffer,
            batch_size,
            max_path_length,
            num_epochs,
            num_eval_steps_per_epoch,
            num_expl_steps_per_train_loop,
            num_trains_per_train_loop,
            num_train_loops_per_epoch=1,
            min_num_steps_before_training=0,
            save_frequency=0,
            conservative=False,
            alpha_ce=0.8
    ):
        super().__init__(
            trainer,
            exploration_env,
            evaluation_env,
            exploration_data_collector,
            evaluation_data_collector,
            replay_buffer,
        )
        self.batch_size = batch_size
        self.max_path_length = max_path_length
        self.num_epochs = num_epochs
        self.num_eval_steps_per_epoch = num_eval_steps_per_epoch
        self.num_trains_per_train_loop = num_trains_per_train_loop
        self.num_train_loops_per_epoch = num_train_loops_per_epoch
        self.num_expl_steps_per_train_loop = num_expl_steps_per_train_loop
        self.min_num_steps_before_training = min_num_steps_before_training
        self.save_frequency = save_frequency
        
        ####################################################################################
        # Added arguments for Conservative Exploration
        ####################################################################################
        self.alpha_ce = alpha_ce
        self.conservative = conservative
        if self.conservative:
            self.history_init_state = []
            self.history_baseline_init_state = []
        
        
    def _train(self):
        
        # Init memory with baseline policy if conservative or the learned policy else
        if self.min_num_steps_before_training > 0:
            if self.conservative:
                init_expl_paths = self.expl_data_collector.collect_new_paths(
                    self.max_path_length,
                    self.min_num_steps_before_training,
                    discard_incomplete_paths=False,
                    safe=True,
                    init=True
                )
            else:
                init_expl_paths = self.expl_data_collector.collect_new_paths(
                    self.max_path_length,
                    self.min_num_steps_before_training,
                    discard_incomplete_paths=False,
                    safe=False,
                    init=True
                )
            self.replay_buffer.add_paths(init_expl_paths)
            self.expl_data_collector.end_epoch(-1)

        for epoch in gt.timed_for(
                range(self._start_epoch, self.num_epochs),
                save_itrs=True,
        ):
            
            # Choose seed to make sure the initial state remains the same
            seed = random.randint(0, 1e6)
                        
            ####################################################################################
            # Conservative Exploration here
            ####################################################################################
               
            if self.conservative:                

                # Retrieve value function of the learned policy and baseline
                lower_policy_value, baseline_value = self.eval_data_collector.init_values(seed)
                self.history_baseline_init_state.append(baseline_value)

                # Add values as attributes for saving later
                self.lower_policy_value = lower_policy_value
                self.baseline_value = baseline_value
                
                print()
                print()
                print('lower bound:', lower_policy_value)
                print('baseline value:', baseline_value)
                print('LHS:', np.sum(self.history_init_state) + lower_policy_value)
                print('RHS:', (1-self.alpha_ce)*np.sum(self.history_baseline_init_state))

                # Check condition
                if np.sum(self.history_init_state) + lower_policy_value >= \
                            (1-self.alpha_ce)*np.sum(self.history_baseline_init_state):
                    play_safe=False
                    self.history_init_state.append(lower_policy_value)
                else:
                    play_safe=True
                    self.history_init_state.append(baseline_value)

                # Logging
                self.conservative_condition_respected = not play_safe
                if self.conservative_condition_respected:
                    print('Play Learned Policy')
                else:
                    print('Play Baseline Policy')
                print()
                print()

                new_paths = self.eval_data_collector.collect_new_paths(
                    self.max_path_length,
                    self.num_eval_steps_per_epoch,
                    discard_incomplete_paths=True,
                    safe=play_safe,
                    seed=seed
                )
                
            else:
                new_paths = self.eval_data_collector.collect_new_paths(
                    self.max_path_length,
                    self.num_eval_steps_per_epoch,
                    discard_incomplete_paths=True,
                    seed=seed
                )
                
            gt.stamp('evaluation sampling')
            
            self.replay_buffer.add_paths(new_paths)
            gt.stamp('data storing', unique=False)
            
            # Reset number of trains
            self.num_trains_per_train_loop = len(new_paths[0]['actions'])

            for _ in range(self.num_train_loops_per_epoch):
                                
                # Comment following lines: we only want the agent to get one path because of the condition the
                # ageent has to respect. Thus, only "self.eval_data_collector.collect_new_paths"
                # will be used
                # new_expl_paths = self.expl_data_collector.collect_new_paths(
                #     self.max_path_length,
                #     self.num_expl_steps_per_train_loop,
                #     discard_incomplete_paths=False,
                #     safe=play_safe,
                #     seed=seed
                # )
                # gt.stamp('exploration sampling', unique=False)

                self.training_mode(True)
                for _ in range(self.num_trains_per_train_loop):
                    train_data = self.replay_buffer.random_batch(
                        self.batch_size)
                    self.trainer.train(train_data)
                gt.stamp('training', unique=False)
                self.training_mode(False)

            self._end_epoch(epoch)
            if self.save_frequency > 0:
                if epoch % self.save_frequency == 0:
                    self.trainer.save_models(epoch)
                    # self.replay_buffer.save_buffer(epoch)
                    
                 
    def get_conservative_condition_diagnostics(self):
        if self.conservative:
            stats = OrderedDict([
                ('conservative', 1),
                ('lower_bound', self.lower_policy_value),
                ('baseline_value', self.baseline_value),
                ('LHS', np.sum(self.history_init_state[:-1]) + self.lower_policy_value),
                ('RHS', (1-self.alpha_ce)*np.sum(self.history_baseline_init_state)),
                ('Conservative_Condition_Respected', self.conservative_condition_respected)
            ])
        else:
            stats = OrderedDict([
                ('conservative', 0)
            ])
        return stats
                 
                    
    def _log_stats(self, epoch):
        """
        Override initial function to add logs regarding the Conservative Exploration condition.
        """
        
        logger.log("Epoch {} finished".format(epoch), with_timestamp=True)

        """
        Replay Buffer
        """
        logger.record_dict(
            self.replay_buffer.get_diagnostics(),
            prefix='replay_buffer/'
        )

        """
        Trainer
        """
        logger.record_dict(self.trainer.get_diagnostics(), prefix='trainer/')

        """
        Conservative Condition
        """
        logger.record_dict(self.get_conservative_condition_diagnostics(), prefix='condition/')
        
        """
        Exploration. No need here since we do not use it
        """
        # logger.record_dict(
        #     self.expl_data_collector.get_diagnostics(),
        #     prefix='exploration/'
        # )
        # expl_paths = self.expl_data_collector.get_epoch_paths()
        # if hasattr(self.expl_env, 'get_diagnostics'):
        #     logger.record_dict(
        #         self.expl_env.get_diagnostics(expl_paths),
        #         prefix='exploration/',
        #     )
        # logger.record_dict(
        #     eval_util.get_generic_path_information(expl_paths),
        #     prefix="exploration/",
        # )
        """
        Evaluation
        """
        logger.record_dict(
            self.eval_data_collector.get_diagnostics(),
            prefix='evaluation/',
        )
        eval_paths = self.eval_data_collector.get_epoch_paths()
        if hasattr(self.eval_env, 'get_diagnostics'):
            logger.record_dict(
                self.eval_env.get_diagnostics(eval_paths),
                prefix='evaluation/',
            )
        logger.record_dict(
            eval_util.get_generic_path_information(eval_paths),
            prefix="evaluation/",
        )

        """
        Misc
        """
        gt.stamp('logging')
        logger.record_dict(_get_epoch_timings())
        logger.record_tabular('Epoch', epoch)
        logger.dump_tabular(with_prefix=False, with_timestamp=False)