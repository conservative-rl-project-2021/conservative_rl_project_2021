# Conservative-SAC

For now, there are two different versions of the code, but they should be merged soon. 

Both codes are taken from rlkit (https://github.com/vitchyr/rlkit). In both codes, the slack is given by `alpha_ce` and the pessimistic parameter by `lambda` (or `lambda_ce`). Both codes use 1 policy and N Q-value estimates run in parallel.

### Cheetah_exps explanations:

This Code is greatly inspired from the Git SUNRISE: A Simple Unified Framework for Ensemble Learning in Deep Reinforcement Learning (https://github.com/pokaxpoka/sunrise).

The main difference with SUNRISE is that, instead of having N policies and N agents, we have 1 policy and N agents. The code is very modular.

Here is the structure of the Cheetah experiments:
- Experiments can be launched with `./scripts/run_conservative_sunrise.sh [env_name] [beta] [temperature] [lambda] [alpha_ce]` with `[alpha_ce]` is the slack for conservative exploration. The script works for both conservative (checking the condition) or non-conservative agents. It is handled with the boolean hyper-parameter `conservative` defined in `example/conservative_sunrise.py`. `beta` is the Bernoulli mean controlling how much the different Q-estimates are trained from the same data. Temperature is uniquely from SUNRISE-SAC.
- The hyper-parameters are chosen in the file `example/conservative_sunrise.py`. 
- The agent and the way it checks the condition, and the way it gathers data is defined in `rlkit/core/conservative_episodic_batch_rl_algorithm.py`. The agent uses a class from `rlkit/samplers/data_collector/one_path_collector.py` to gather data, a class we had to create because the training was with epochs (usually tens of episodes) while we want to keep the episodic structure. This sampler use functions from `rlkit/samplers/rollout_functions.py`
- The training part is defined in `rlkit/torch/neurips21_sac_conservative.py`. We took the most of SUNRISE enhancements while keeping the structure 1 policy and N agents.
- The visualization of the logs can be seen in the Jupyter Notebook file in `examples/Test_Baseline.ipynb`.

### Hopper_exps explanations:

This code is a modification of the initial SAC code from https://github.com/vitchyr/rlkit.

Download and unzip episodic_SAC.zip or Conservative_SAC.zip.

In episodic_SAC, trajectory samplers are modified to only sample one episode per call, instead of a fix number of steps.

In conservative SAC, the main difference with SAC is that before each exploration sampling, we evaluate N Q-values of the exploration policy using N independant learner trained on different batches of data and boostrapped from previous evaluations of Q functions.

Here is the structure of the Hopper experiments:
- Experiments can be launched with `$ ./scripts/run_sac.sh` from respective folders
- The differents parameters "env_name" + (conservative version) "lambda": conservative factor,  "alpha_ce":slack for conservative exploration - have to be set manually in the file `./example/sac.py`.
- The hyper-parameters are chosen in the file cexample/conservative_sunrise.py`. 
- The agent and the way it checks the condition, and the way it gathers data is defined in `rlkit/core/batch_rl_algorithm.py`. The agent uses a class from `rlkit/samplers/data_collector/path_collector.py` to collect data.
- The training part is defined in `rlkit/torch/sac/sac.py`.
- To visualize the experiments, use `./plot.py` (and verify the paths of non-conservative runs and consevative runs logs on top of the file).

For more details about the algorithm and installation procedure see Readme.txt in episodic_SAC.zip or Conservative_SAC.zip 
